požadavky na balíčky: __future__, copy, csv (vše součástí standardní instalace Python)

atributy Matrix:
- matrix: list (2D seznam představující matici, tj. seznam jednotlivých řádků matice, každý řádek je také seznam)
- num_of_rows: int (počet řádků)
- num_of_columns: int (počet sloupců)

hlavní metody Matrix:
- transpose(self, inplace: bool = False) -> Matrix (transponuje matici)
- multiply_row_by_num(self, row_index: int, scalar: float = 1) -> int (vynásobí řádek matice číslem)
- swap_rows(self, first_row_index: int, second_row_index: int) -> int (prohodí řádky)
- add_rows(self, target_row_index: int, source_row_index: int, scalar: float = 1) -> None (sečte dva řádky)
- upper_triangularize(self, inplace: bool | None = False) -> int (převede matici do horního trojúhelníkového tvaru)
- lower_triangularize(self, inplace: bool = False) -> int (převede matici do dolního trojúhelníkového tvaru)
- get_linear_independance(self) -> bool (rozhodne o lineární (ne)závislosti sloupců matice)
- @staticmethod multiply(matrix_a: Matrix, matrix_b: Matrix) -> Matrix (vynásobí dvě matice)

pomocné metody Matrix: 
- _code_init(self, matrix: list) -> None (načte matici z kódu, tzn. uživatel už při vytváření objektu Matrix předal 2D seznam)
- _terminal_init(self, num_of_rows: int = None) -> None (načte matici z terminálu od uživatele)
- _request_row(self, first: bool = False) -> list (postupně si žádá od uživatele řádky matice, využito v _terminal_init)
- _file_init(self, file_path: str) -> None (načte matici z CSV souboru)
- _reset_matrix(self, inplace: bool, temp: Matrix) -> None (resetuje matici do původního tvaru)
- __str__(self) -> str
- @staticmethod _print_matrix(matrix: list, num_of_rows: int = 0, num_of_columns: int = None) -> str (vrátí matici v čitelném a zarovnaném tvaru)


atributy SquareMatrix(Matrix):
- zděděné atributy Matrix
- order: int (řád matice)
- unit_matrix: list (jednotková matice přiřazená ke čtvercové, využito při Gaussově eliminaci)

hlavní metody SquareMatrix:
- zděděné metody Matrix (metody pro ekvivalentní řádkové úpravy navíc provádějí tyto úpravy i na jednotkové matici)
- reset_unit(self) -> None (resetuje jednotkovou matici)
- get_determinant(self, inplace: bool = False) -> int (výpočet determinantu)
- is_invertable(self, inplace: bool | None = False) -> bool (rozhodne o tom, zda je matice regulární nebo singulární)
- full_gaussian(self, inplace: bool = False) -> SquareMatrix (Gaussova eliminace k určení jednotkové matice)
- print_unit(self) -> str (vrací jednotkovou matici v čitelném tvaru)

pomocné metody SquareMatrix:
- zděděné metody Matrix 
- _reset_matrix(self, inplace: bool, temp: SquareMatrix) -> None (resetuje matici do původního tvaru včetně té jednotkové)
