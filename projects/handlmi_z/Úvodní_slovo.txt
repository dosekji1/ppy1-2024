Toto je pouze úvodní slovo k zápočtové úloze
Vzhledem k tomu, že bych jednoho (snad ne tolik vzdáleného) dne chtěl být datový analytik, rozhodl jsem se, že vytvořím jednoduchý program, který pomocí objektového progrmaování a NumPy zpracuje jednoduchý dataset.
To mi však přišlo málo, a tak jsem se rozhodl zakomponovat do toho malý krok, který alespoň lehce zvisualizuje data. 
Přeci jen, to že medián je 7 je hezký, ale bez dalších informací celkem k ničemu.
Dataset jsem získal z https://archive.ics.uci.edu/dataset/186/wine+quality
